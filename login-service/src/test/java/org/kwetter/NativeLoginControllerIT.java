package org.kwetter;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeLoginControllerIT extends LoginControllerTest {

    // Execute the same tests but in native mode.
}