package org.kwetter.util;

import io.smallrye.jwt.build.Jwt;
import io.smallrye.jwt.build.JwtClaimsBuilder;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.enterprise.context.ApplicationScoped;
import java.util.Arrays;
import java.util.HashSet;

@ApplicationScoped
public class JwtGenerator {




    public static String generateToken(String id) {
        String token = Jwt.issuer("https://kwetter.com/issuer")
                .upn("jdoe@quarkus.io")
                .claim("uid", id)
                .groups(new HashSet<>(Arrays.asList("User", "Admin")))
                .sign();
        return token;
    }
}
