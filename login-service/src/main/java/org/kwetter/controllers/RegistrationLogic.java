package org.kwetter.controllers;

import org.kwetter.models.ApiReponse;
import org.kwetter.models.User;
import org.kwetter.servicelayer.KafkaServiceLayer;
import org.kwetter.servicelayer.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class RegistrationLogic {
    //private final UserService userService;
    private final KafkaServiceLayer kafkaService;

    public RegistrationLogic(KafkaServiceLayer kafkaServiceLayer){
        //this.userService = new UserService();
        this.kafkaService = kafkaServiceLayer;
    }

    public ApiReponse registerNewUser(User user) {
        ApiReponse response = new ApiReponse();
        if (checkValues(user)){
            response.header = HttpStatus.BAD_REQUEST;
            return response;
        }

        try {
            user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
            kafkaService.sendUserToKafka(user);
            response.header = HttpStatus.CREATED;
            return response;
        }catch(Exception e){
            e.printStackTrace();
            response.header = HttpStatus.INTERNAL_SERVER_ERROR;
            return response;
        }
    }

    private boolean checkValues(User user) {
        return (user.password == null || user.email == null || user.username == null);
    }


}