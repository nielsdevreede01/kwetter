package org.kwetter.controllers;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.kwetter.logic.LoginLogic;
import org.kwetter.models.ApiReponse;
import org.kwetter.models.User;
import org.kwetter.servicelayer.KafkaServiceLayer;
import org.kwetter.servicelayer.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


@RestController("/api")
public class LoginController {

    final
    UserService userService;

    LoginLogic loginLogic;
    RegistrationLogic registrationLogic;

    public LoginController(KafkaServiceLayer kafkaServiceLayer, @RestClient UserService userService){
        this.loginLogic = new LoginLogic(userService);
        this.registrationLogic = new RegistrationLogic(kafkaServiceLayer);
        this.userService = userService;
    }


    @PostMapping("login")
    public ApiReponse GetLoginToken(@RequestBody User user){
        return loginLogic.loginWithCredentials(user);
    }


    @PostMapping("/register")
    public ApiReponse RegisterNewUser(@RequestBody User user){
        return registrationLogic.registerNewUser(user);
    }
}