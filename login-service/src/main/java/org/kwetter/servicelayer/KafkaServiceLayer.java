package org.kwetter.servicelayer;


import com.google.gson.Gson;
import io.smallrye.mutiny.Multi;
import io.smallrye.reactive.messaging.kafka.Record;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.kwetter.models.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.Duration;

@ApplicationScoped
public class KafkaServiceLayer {

    @Inject
    @Channel("registration")
    Emitter<String> emitter;


    public void sendUserToKafka(User user){
        Gson gson = new Gson();
        String jsonString = gson.toJson(user);
        System.out.println(jsonString);
        emitter.send(jsonString);
    }
}
