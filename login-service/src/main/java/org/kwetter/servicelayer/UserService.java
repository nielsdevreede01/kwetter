package org.kwetter.servicelayer;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.kwetter.models.User;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@RegisterRestClient(configKey = "user-service")
@ApplicationScoped
public interface UserService {

    @GET
    @Path("/email/{email}")
    User getUserByEmail(@PathParam(value = "email") String email);

    @GET
    @Path("/username/{username}")
    User getUserByUsername(@PathParam(value = "username") String username);

}
