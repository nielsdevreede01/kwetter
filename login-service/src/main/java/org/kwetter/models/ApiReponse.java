package org.kwetter.models;

import io.netty.handler.codec.http.HttpResponseStatus;
import org.springframework.http.HttpStatus;

public class ApiReponse {
    public HttpStatus header;
    public String body;
    public String token;
}
