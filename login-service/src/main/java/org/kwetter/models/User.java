package org.kwetter.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User{
    public String id;
    public String username;
    public String email;
    public String password;
    public Role role;
}
