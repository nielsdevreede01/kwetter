package org.kwetter.logic;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.kwetter.models.ApiReponse;
import org.kwetter.models.User;
import org.kwetter.servicelayer.UserService;
import org.kwetter.util.JwtGenerator;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import javax.inject.Inject;

public class LoginLogic {

    private UserService userService;

    public LoginLogic(UserService s) {
        this.userService = s;
    }

    public ApiReponse loginWithCredentials(User user) {
        ApiReponse response = new ApiReponse();
        User retrievedUser = getUserByEmailOrUsername(user);
        if (retrievedUser == null) {
            return null;
        }

        if (BCrypt.checkpw(user.password, retrievedUser.password)) {
            response.token = JwtGenerator.generateToken(retrievedUser.id);
            response.header = HttpStatus.OK;
            return response;
        }
        response.header = HttpStatus.UNAUTHORIZED;
        return response;
    }

    private User getUserByEmailOrUsername(User user) {
        if (user.email != null) {
            return userService.getUserByEmail(user.email);

        } else if (user.username != null) {
            return userService.getUserByUsername(user.username);
        }
        return null;
    }
}
