package org.kwetter.Controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.context.ThreadContext;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.kwetter.Logic.KweetLogic;
import org.kwetter.Models.Kweet;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.CompletionStage;

@ApplicationScoped
public class KweetKafkaConsumer {

    private final KweetLogic logic;

    public KweetKafkaConsumer() {
        logic = new KweetLogic();
    }

    @Incoming("kweet")
    @Transactional
    public CompletionStage<?> receive(String kweet) {
        ManagedExecutor executor = ManagedExecutor.builder()
                .maxAsync(5)
                .propagated(ThreadContext.CDI,
                        ThreadContext.TRANSACTION)
                .build();
        ThreadContext threadContext = ThreadContext.builder()
                .propagated(ThreadContext.CDI,
                        ThreadContext.TRANSACTION)
                .build();

        return executor.runAsync(threadContext.contextualRunnable(() -> {
            try {
                JsonObject jobj = new Gson().fromJson(kweet, JsonObject.class);
                Kweet newKweet = new Kweet();
                newKweet.setId(UUID.randomUUID().toString());
                newKweet.setBody(jobj.get("body").getAsString());
                newKweet.setLikes(0);
                newKweet.setTimestamp(Date.from(Instant.now()));
                logic.saveKweet(newKweet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
    }
}

