package org.kwetter.Controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.context.ThreadContext;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.kwetter.Logic.KweetLogic;
import org.kwetter.Models.Comment;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.UUID;
import java.util.concurrent.CompletionStage;

@ApplicationScoped
public class CommentKafkaConsumer {

    private final KweetLogic logic;

    public CommentKafkaConsumer() {
        logic = new KweetLogic();
    }

    @Incoming("comment")
    @Transactional
    public CompletionStage<?> receive(String comment) {
        ManagedExecutor executor = ManagedExecutor.builder()
                .maxAsync(5)
                .propagated(ThreadContext.CDI,
                        ThreadContext.TRANSACTION)
                .build();
        ThreadContext threadContext = ThreadContext.builder()
                .propagated(ThreadContext.CDI,
                        ThreadContext.TRANSACTION)
                .build();

        return executor.runAsync(threadContext.contextualRunnable(() -> {
            try {
                JsonObject jobj = new Gson().fromJson(comment, JsonObject.class);
                Comment newComment = new Comment();
                newComment.setId(UUID.randomUUID().toString());
                newComment.setBody(jobj.get("body").getAsString());
                newComment.setUserId(jobj.get("userId").getAsString());
                logic.saveComment(newComment, jobj.getAsJsonObject("kweet").get("id").getAsString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
    }
}