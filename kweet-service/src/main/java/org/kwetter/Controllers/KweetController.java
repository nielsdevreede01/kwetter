package org.kwetter.Controllers;


import org.kwetter.Logic.KweetLogic;
import org.kwetter.Models.Comment;
import org.kwetter.Models.Kweet;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController()
public class KweetController {

    @Inject
    CommentKafkaConsumer commentKafkaConsumer;

    @Inject
    KweetKafkaConsumer kweetKafkaConsumer;


    private final KweetLogic logic;
    public KweetController(KweetLogic logic){
        this.logic = logic;
    }

    @GetMapping("/search/{searchText}")
    public Object TestDatabase(@PathVariable("searchText") String searchText){
        System.out.println("Searching for: " + searchText);
        return logic.getKweetsFromSearch(searchText);
    }

    @GetMapping("/kweet/user/{userId}")
    public List<Kweet> getKweetsByUserId(@PathVariable("userId") String userId){
        return logic.getKweetsByUserId(userId);
    }

    @PostMapping("/new/kweet")
    public Object NewKweet(@RequestBody Kweet newKweet){
        logic.saveKweet(newKweet);
        return HttpStatus.OK;
    }

    @PostMapping("/new/comment/{kweetId}")
    public Object NewComment(String kweetId, @RequestBody Comment comment){
        logic.saveComment(comment, kweetId);
        return HttpStatus.OK;
    }

    @GetMapping("/search/kweet/{search}")
    public List<Kweet> searchKweet(@PathVariable("search") String search){
        return logic.getKweetsFromSearchHibernate(search);
    }
}
