package org.kwetter.Logic;

import org.hibernate.search.mapper.orm.session.SearchSession;
import org.kwetter.Models.Comment;
import org.kwetter.Models.Kweet;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Transactional
@Service
public class KweetLogic {


    @Inject
    SearchSession searchSession;

    public KweetLogic(){
    }

    public List<Kweet> getKweetsFromSearch(String searchText){
        System.out.println(System.currentTimeMillis());
        var res = searchSession.search(Kweet.class)
                .where(f ->
                        searchText == null || searchText.trim().isEmpty() ?
                                f.matchAll() :
                                f.simpleQueryString()
                                        .fields("body").matching(searchText))
                .fetchAllHits();
        System.out.println(System.currentTimeMillis());
        return res;
    }

    public List<Kweet> getKweetsFromSearchHibernate(String searchText){
        System.out.println(System.currentTimeMillis());
        List<Kweet> res = Kweet.list("body LIKE ?1", searchText);
        System.out.println(System.currentTimeMillis());
        return res;
    }




    public void saveKweet(Kweet kweet){
        kweet.setId(UUID.randomUUID().toString());
        kweet.persist();
    }

    public void saveComment(Comment comment, String kweetId) {
        comment.kweet.setId(kweetId);
        comment.persist();
    }

    public List<Kweet> getKweetsByUserId(String userId) {
        return Kweet.findByUserId(userId);
    }
}
