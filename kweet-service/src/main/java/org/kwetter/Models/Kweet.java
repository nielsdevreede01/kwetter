package org.kwetter.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import java.util.Date;
import java.util.List;

@Entity
@Indexed
@Getter
@Setter
public class Kweet extends PanacheEntityBase {

    @Id
    private String id;
    private Date timestamp;
    private int likes;
    private String userId;

    @FullTextField(analyzer = "name")
    private String body;

    public static List<Kweet> findByUserId(String userId) {
        return list("userId", userId);
    }
    public static List<Kweet> findByBody(String body) { return list("body", body);}
}
