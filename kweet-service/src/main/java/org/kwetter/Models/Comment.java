package org.kwetter.Models;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Indexed;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Indexed
@Getter
@Setter
public class Comment extends PanacheEntityBase {

    @Id
    public String id;
    public String body;
    public String userId;

    @ManyToOne
    public Kweet kweet;
}
