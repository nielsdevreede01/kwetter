import HelloWorld from "@/components/HelloWorld.vue";
import { mount } from "@vue/test-utils";

describe("HelloWorld.vue", () => {
  const wrapper = mount(HelloWorld);
  const msg = wrapper.get('[data-test="hello-world"]');
  console.log("data msg is", msg);

  it("checks textcontent to Hello world! ", () => {
    expect(msg.text()).toBe("Hello world!");
  });
});
