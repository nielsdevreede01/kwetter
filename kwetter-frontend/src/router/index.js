import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../components/HelloWorld.vue";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import Kweets from "../views/Kweets.vue";
import NewKweet from "../views/NewKweet.vue";
import SearchKweet from "../views/SearchKweet.vue";
import PrivacyPolicy from "../views/PrivacyPolicy.vue";

const routes = [
  {
    path: "/",
    name: "login",
    component: Login,
    meta: {
      guest: true
    }
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: {
      guest: true
    }
  },
  {
    path: "/privacy",
    name: "privacy",
    component: PrivacyPolicy,
    meta: {
      guest: true
    }
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
    meta: {
      guest: true
    }
  },
  {
    path: "/home",
    name: "Home",
    component: Kweets,
    meta: {
      guest: true
    }
  },
  {
    path: "/newKweet",
    name: "NewKweet",
    component: NewKweet,
    meta: {
      guest: true
    }
  },
  {
    path: "/search/kweet",
    name: "Searchkweet",
    component: SearchKweet,
    meta: {
      guest: true
    },
    props: (route) => ({ search: route.query.search })
  },
];
const router = createRouter({
  history: createWebHashHistory(),
  routes,
});


router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.token != null) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

export default router;