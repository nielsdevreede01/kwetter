import { createStore } from "vuex";

export default createStore({
  state: {
    //store data like username

    userToken: null
  },
  mutations: {
      setLoggedIn(state){
        state.userToken = localStorage.token
      }
      


    //methods that change data in the states
  },
  actions: {
    //
  },
  getters:{
    get_token(state){
      return state.userToken
    }
    // can give data in state but change the data before ginving it to the user
  },
  modules: {
  }
})
