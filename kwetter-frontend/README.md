# Rekeningrijden

This Vue.js 6 project shows the road users and their trips.
It is run by having NPM and nodejs (latest version 15.x, not lts) installed. Once that's
done run the stack by;

```sh
npm install && npm run dev
```

this will install dependencies once
after that just run `npm run dev`

To create a build for the pipeline do

```sh
npm run build
```

# Testing

For unit test we use Jest and [https://next.vue-test-utils.vuejs.org/guide/](https://next.vue-test-utils.vuejs.org/guide/)

do

```sh
npm run test
```

for end-to-end tests we use Cypress.io
to run e2e tests do

```sh
npm run e2e
```
