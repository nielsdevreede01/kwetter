package com.kwetter.Kafka;

import com.google.gson.Gson;
import com.kwetter.Models.Comment;
import com.kwetter.Models.Kweet;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Outgoing;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class KafkaProducer {

    @Inject
    @Channel("kweet")
    Emitter<String> kweetEmitter;


    @Inject
    @Channel("kweet")
    Emitter<String> commentEmitter;

    public void createNewKweet(Kweet kweet){
        Gson gson = new Gson();
        String jsonString = gson.toJson(kweet);
        kweetEmitter.send(jsonString);
    }


    public void createNewComment(Comment comment){
        Gson gson = new Gson();
        String jsonString = gson.toJson(comment);
        commentEmitter.send(jsonString);
    }
}
