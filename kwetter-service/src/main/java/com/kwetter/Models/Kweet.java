package com.kwetter.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;
import java.util.Date;

@Getter
@Setter
public class Kweet implements Comparable<Kweet> {

    public String id;
    public Date timestamp;
    public int likes;
    public String userId;
    public String body;
    public User user;


    public Kweet(String kweetId) {
        this.id = kweetId;
    }

    public Kweet(){}

    @Override
    public int compareTo(Kweet o) {
        return getTimestamp().compareTo(o.getTimestamp());
    }
}
