package com.kwetter.Models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Comment {
    private String id;
    private String body;
    private String userId;
    private Kweet kweet;
}

