package com.kwetter.Models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    public String id;
    public String username;
    public String email;
    public String password;
    public Role role;
}
