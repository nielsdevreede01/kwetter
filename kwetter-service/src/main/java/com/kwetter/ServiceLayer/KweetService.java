package com.kwetter.ServiceLayer;


import com.kwetter.Models.Kweet;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.springframework.web.bind.annotation.PathVariable;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;

@RegisterRestClient(configKey = "kweet-service")
@ApplicationScoped
public interface KweetService {

    @GET
    @Path("/kweet/user/{userId}")
    List<Kweet> getKweetsFromUser(@PathParam(value = "userId") String userId);

    @GET
    @Path("/search/kweet/{searchText}")
    List<Kweet> getKweetsBasedOnSearch(@PathParam(value = "searchText") String searchText);
}
