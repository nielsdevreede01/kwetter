package com.kwetter.ServiceLayer;

import com.kwetter.Models.User;
import lombok.Getter;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.List;

@RegisterRestClient(configKey = "user-service")
@ApplicationScoped
public interface UserService {

    @GET
    @Path("/following/{userId}")
    List<User> getFollowingUsersByUserId(@PathParam(value = "userId") String userId);

    @GET
    @Path("uuid/{userId}")
    User getUserByUserId(@PathParam(value = "userId") String userId);
}
