package com.kwetter.Controllers;

import com.google.gson.Gson;
import com.kwetter.Kafka.KafkaProducer;
import com.kwetter.Models.Comment;
import com.kwetter.Models.Kweet;
import com.kwetter.Models.User;
import com.kwetter.ServiceLayer.KweetService;
import com.kwetter.ServiceLayer.UserService;
import com.kwetter.Util.JwtDecryptor;

import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import io.smallrye.jwt.auth.principal.ParseException;

import javax.inject.Inject;
import java.util.*;

@RestController()
@RequestMapping("/api/kwetter")
public class KwetterController {

    @Inject
    KafkaProducer kafkaProducer;

    @Inject
    @RestClient
    UserService userService;

    @Inject
    @RestClient
    KweetService kweetService;



    Gson gson;

    @GetMapping("kweet/me")
    public Object getMyKweets(@RequestHeader("Authorization") String token) throws ParseException, InvalidJwtException {
        JsonWebToken jwt = JwtDecryptor.decodeTokenUnverified(token);
        String uid = jwt.getClaim("uid");
        List<User> followingUsers = userService.getFollowingUsersByUserId(uid);
        List<Kweet> kweets = new ArrayList<>();
        for (User u : followingUsers) {
            kweets.addAll(kweetService.getKweetsFromUser(u.getId()));
            for (Kweet k : kweets) {
                k.setUser(u);
            }
        }
        Collections.sort(kweets);
        return kweets;
    }

    @PostMapping("kweet")
    public Object createNewKweet(@RequestBody Kweet kweet) {
        if (kweet.getBody() == null) {
            return HttpStatus.BAD_REQUEST;
        }
        kafkaProducer.createNewKweet(kweet);
        return HttpStatus.OK;
    }

    @PostMapping("comment/{kweetId}")
    public Object createNewComment(@RequestParam String kweetId, @RequestBody Comment comment) {
        if (kweetId == null || comment.getBody() == null || comment.getUserId() == null) {
            return HttpStatus.BAD_REQUEST;
        }
        Kweet k = new Kweet(kweetId);
        comment.setKweet(k);
        kafkaProducer.createNewComment(comment);
        return HttpStatus.OK;
    }

    @GetMapping("search/kweet/{searchText}")
    public List<Kweet> searchForKweet(@PathVariable("searchText") String searchText){
        var kweets = kweetService.getKweetsBasedOnSearch(searchText);
        for (Kweet kweet: kweets) {
            kweet.setUser(userService.getUserByUserId(kweet.getUserId()));
        }
        return kweets;
    }
}
