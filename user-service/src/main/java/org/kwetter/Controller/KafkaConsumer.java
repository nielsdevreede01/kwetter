package org.kwetter.Controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.context.ThreadContext;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.kwetter.Logic.UserLogic;
import org.kwetter.Models.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.concurrent.CompletionStage;

@ApplicationScoped
public class KafkaConsumer {


    private final UserLogic logic;

    public KafkaConsumer() {
        logic = new UserLogic();
    }


    @Incoming("registration")
    @Transactional
    public CompletionStage<?> receive(String user) {
        ManagedExecutor executor = ManagedExecutor.builder()
                .maxAsync(5)
                .propagated(ThreadContext.CDI,
                        ThreadContext.TRANSACTION)
                .build();
        ThreadContext threadContext = ThreadContext.builder()
                .propagated(ThreadContext.CDI,
                        ThreadContext.TRANSACTION)
                .build();

        return executor.runAsync(threadContext.contextualRunnable(() -> {
            try {
                JsonObject jobj = new Gson().fromJson(user, JsonObject.class);
                User newUser = new User();
                newUser.setEmail(jobj.get("email").getAsString());
                newUser.setUsername(jobj.get("username").getAsString());
                newUser.setPassword(jobj.get("password").getAsString());
                logic.registerNewUser(newUser);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
    }
}
