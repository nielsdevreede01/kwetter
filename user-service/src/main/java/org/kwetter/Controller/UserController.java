package org.kwetter.Controller;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import org.kwetter.Controller.KafkaConsumer;
import org.kwetter.Logic.UserLogic;
import org.kwetter.Models.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.ws.rs.Path;
import java.util.List;

@RestController()
public class UserController {

    @Inject
    UserLogic logic;
    @Inject
    MeterRegistry registry;

    @GetMapping("/email/{email}")
    public User getUserByEmail(@PathVariable(value = "email") String email){
        registry.counter("user_email", Tags.of("email", email)).increment();
        return logic.getUserByEmail(email);
    }

    @GetMapping("/username/{username}")
    public User getUserByUsername(@PathVariable(value = "username") String username){
        registry.counter("user_username", Tags.of("name", username)).increment();
        return logic.getUserByUsername(username);
    }

    @GetMapping("/uuid/{uid}")
    public User getUserByUuid(@PathVariable(value = "uid") String uid){
        return logic.getUserByUuid(uid);
    }

    @GetMapping("/following/{userId}")
    public List<User> getListOfFollowingUserByUid(@PathVariable(value = "userId") String userId){
        return logic.getListOfFollowingUserByUid(userId);
    }
}

