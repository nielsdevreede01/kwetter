package org.kwetter.Logic;

import org.kwetter.Models.Following;
import org.kwetter.Models.User;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.control.ActivateRequestContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
@Transactional
@ActivateRequestContext
public class UserLogic {

    @Inject
    EntityManager em;

    public void registerNewUser(User user){
        user.setId(UUID.randomUUID().toString());
        user.persist();
        System.out.println("Registered user");
    }

    public User getUserByEmail(String email) {
        return User.findByEmail(email);
    }

    public User getUserByUsername(String username) {
        return User.findByUsername(username);
    }

    public User getUserByUuid(String uid) {
        return User.findById(uid);
    }

    public List<User> getListOfFollowingUserByUid(String userId) {
        List<Following> followings = Following.findByUserId(userId);
        List<User> users = new ArrayList<>();
        for (Following f : followings) {
            users.add(f.getIsFollowingUser());
        }
        return users;
    }
}
