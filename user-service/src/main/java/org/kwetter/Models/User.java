package org.kwetter.Models;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User extends PanacheEntityBase {

    @Id
    private String id;
    private String username;
    private String email;
    private String password;
    private String bio;
    private String location;
    private String websiteUrl;

    public static User findByEmail (String email){
        return find("email", email).firstResult();
    }

    public static User findByUsername(String username) {
        return find("username", username).firstResult();
    }
}
