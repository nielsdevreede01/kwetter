package org.kwetter.Models;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "following")
public class Following extends PanacheEntityBase {

    public Following (){
        this.timestamp = new Date();
    }
    @Id
    public String id;

    @ManyToOne
    public User user;

    @ManyToOne
    public User isFollowingUser;
    public Date timestamp;


    public static List<Following> findByUserId (String userId){
        return list("user_id", userId);
    }
}
