public class LogInfo{
    public string id {get;set;}
    public string origin {get;set;}
    public Severity severity {get;set;}
    public string body {get;set;}
    public string userId {get;set;}


}

public enum Severity {
    SEVERE,
    WARNING,
    ERROR,
    INFORMATION
}