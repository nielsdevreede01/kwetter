using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Data.SqlClient;

namespace Kwetter
{
    public static class create_log
    {
        [FunctionName("create_log")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            LogInfo info;
            using (StreamReader stream = new StreamReader(req.Body))
            {
                try
                {
                    info = JsonConvert.DeserializeObject<LogInfo>(stream.ReadToEnd());
                }
                catch (Exception e)
                {
                    log.LogError(e.Message);
                    return new BadRequestResult();
                }
            }

            if (info.body  == null || info.origin == null){
                return new BadRequestResult();
            }

            
            var str =   "Server=tcp:nielsdevreede.database.windows.net,1433;Initial Catalog=kwetter-monitoring;Persist Security Info=False;User ID=nielsdevreede;Password=G684b1228;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;";

            Console.WriteLine(str);
            using (SqlConnection conn = new SqlConnection(str))
            {   
                conn.Open();
                info.id = Guid.NewGuid().ToString();
                var query = $"insert into logs (id, userid, body, timestamp_log, severity, origin) values ('{info.id}', '{info.userId}', '{info.body}', '{DateTime.Now}', '{info.severity}', '{info.origin}'); ";
                Console.WriteLine(query);
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    await cmd.ExecuteNonQueryAsync();
                }
            }
            return new OkObjectResult(info);
        }
    }
}
